Source: luma.core
Maintainer: Anton Gladky <gladk@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               fonts-dejavu,
               python3-cbor2,
               python3-deprecated,
               python3-dev,
               python3-pil,
               python3-smbus2,
               python3-ftdi,
               python3-pytest,
               python3-setuptools,
               sphinx
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/luma.core
Vcs-Git: https://salsa.debian.org/debian/luma.core.git
Homepage: https://github.com/rm-hull/luma.core
Testsuite: autopkgtest-pkg-python

Package: python3-luma.core
Architecture: any
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         python3-ftdi,
         python3-smbus2
Description: component library providing a Pillow-compatible drawing canvas
 other functionality to support drawing primitives and text-rendering
 capabilities for small displays on the Raspberry Pi and other single board
 computers:
  * scrolling/panning capability,
  * terminal-style printing,
  * state management,
  * color/greyscale (where supported),
  * dithering to monochrome,
  * sprite animation,
  * flexible framebuffering (depending on device capabilities)

Package: luma.core-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends},
         ${misc:Depends},
Description: doc for component library providing a Pillow-compatible drawing canvas
 other functionality to support drawing primitives and text-rendering
 capabilities for small displays on the Raspberry Pi and other single board
 computers:
  * scrolling/panning capability,
  * terminal-style printing,
  * state management,
  * color/greyscale (where supported),
  * dithering to monochrome,
  * sprite animation,
  * flexible framebuffering (depending on device capabilities)
  Package contains documentation
